package com.asd.exceptions;

public class WrongPriceException extends Exception {

	public WrongPriceException(String message) {
		super(message);
	}

}
