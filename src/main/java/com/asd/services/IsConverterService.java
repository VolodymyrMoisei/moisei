package com.asd.services;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public interface IsConverterService {

	/**
	 * Converts Integer value into String. If provided value is null, should return null too.
	 *
	 * @param value of Integer
	 * @return string value or null
	 */
	String convertIntegerIntoString(Integer value);

	/**
	 * Converts String value into Integer.
	 *
	 * @param value as string
	 * @return integer value
	 */
	Integer convertStringIntoInteger(String value);

	/**
	 * Converts String value into Double.
	 *
	 * @param value as string
	 * @return double value
	 */
	Double convertStringIntoDouble(String value);

	/**
	 * Converts string value into {@link LocalDate}. Should return null if null parameter was provided.
	 * Provided value should be in the next format: "1970-01-01"
	 *
	 * @param dateString string value of date
	 * @return date value in {@link LocalDate} format
	 * @throws DateTimeParseException in case when provided string date is incorrect
	 */
	LocalDate convertStringIntoLocalDate(String dateString) throws DateTimeParseException;

}
