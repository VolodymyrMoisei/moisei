package com.asd.entities;

public interface IsTax {

	Integer getId();

	Integer getEntityId();

	Double getAmount();

	String getEntityType();

	Integer getProductId();

	Integer getPartyId();

}
