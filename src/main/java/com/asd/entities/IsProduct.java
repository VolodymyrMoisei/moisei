package com.asd.entities;

import com.asd.constants.ProductState;

public interface IsProduct {

	String getName();

	ProductState getState();

}
